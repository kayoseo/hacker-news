var express=require('express');
var NewsController=require('../controllers/news');

var router= express.Router();


router.post('/saveNews',NewsController.saveNews);
router.post('/deleteNews',NewsController.deleteNews);

module.exports=router;