var mongoose = require('mongoose');
var app = require('./app');
var port = 3700;


mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/hackerNews')
    .then(() => {
        console.log("Conexion Exitosa...");
        app.listen(port, () => { console.log("run server localhost:3700"); })
    })
    .catch(err => console.log(err));