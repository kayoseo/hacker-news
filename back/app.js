var express = require('express');
var bodyParser = require('body-parser');

var app = express();

//rutas
var news_routes = require('./routes/news');


//middlewares

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Cors

// rutas
app.use('/api',news_routes);
//exportar 
module.exports = app;