var News = require('../models/news');
var NewsDelete = require('../models/newsDelete');

var controller =
{

    saveNews: function (req, res) {
        var news = new News();
        var newsDelete = new NewsDelete();

        var params = req.body;
        news.title = params.title;
        news.story_id=params.story_id;

        NewsDelete.find({ story_id: params.story_id }, function (errFind, docs) {
            if(errFind)
            {
                console.log("Hubo un error", errFind);
            }
            if (docs.length==0) {
                console.log("no existe la noticia");
                news.save((err, newsNew) => {

                    if (err) {
                        console.log("error al guardar");
                        return res.status(500).send({ message: 'Error al guardar.' })
                    };
                    if (!newsNew) {
                        console.log("No se ha podido guardar");
                        return res.status(404).send({ message: 'No se ha podido guardar la noticia' })
                    };

                    return res.status(200).send({status: 'succes', news: newsNew });
                })
            }
            else
            {
                return res.status(200).send();
            }
            
        });


        /* return res.status(200).send({
            news: news
        }) */
    },

    deleteNews: function (req, res) {
        var newsDelete = new NewsDelete();

        var params = req.body;
        newsDelete.story_id = params.story_id;
        newsDelete.save((err, newsDelete) => {
        
        });
        return res.status(200).send({
            news: newsDelete
        })
    }

};

module.exports = controller;