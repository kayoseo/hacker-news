var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var newsDeleteSchema = Schema({
    story_id: Number
});

module.exports=mongoose.model('NewsDelete',newsDeleteSchema);