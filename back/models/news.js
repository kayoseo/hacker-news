var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var newsSchema = Schema({
    created_at: Date,
    title: String,
    url: String,
    author: String,
    story_id: Number,
    story_title: String,
    story_url: String,
    created_at_i: Number,
});

module.exports=mongoose.model('News',newsSchema);